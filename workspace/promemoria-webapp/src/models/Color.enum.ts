export enum Color {
  Red = '#e53935',
  Orange = '#fb8c00',
  Yellow = '#fdd835',
  Green = '#43a047',
  Lightblu = '#039be5',
  Blu = '#1e88e5',
  Purple = '#8e24aa',
  Pink = '#ea80fc',
  Brown = '#6d4c41',
  Grey = '#546e7a',
}
