
interface IReminder {
  name: string;
  date?: string;
  description?: string;
  done?: boolean;
}
//questo è un oggetto ts. Per costruirlo, devo mettere nome e ordine
//Reminder("stringaConNome", 1ordine, +descrizne facoltativa, + check facoltativo)

export class Reminder implements IReminder {
  name: string;
  date?: string;
  description?: string;
  done?: boolean;
  id: string = parseInt(Math.random() * 10000 + '', 10) + '';

  constructor(obj: IReminder) {
    this.name = obj.name;
    this.date = obj.date;
    this.description = obj.description;
    this.done = obj.done;
  }
}
