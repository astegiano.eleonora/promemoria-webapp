import {Color} from './Color.enum';
import {Reminder} from './Reminder';

export interface IList {
  name: string;
  priority: number;
  color: Color;
  
}
//export default IList;
export class List {
  id: string;
  name: string;
  color: Color;
  reminders: Reminder[];

  //sto creando un oggetto di nome classe
  //il costruttore ha bisogno di nome, priorità, colore e reminder da inserire
  //new List("nomeLista", priorità, colore.quelloCheVoglio, "cosa devo ricordare?")

  constructor(  name: string,
                color: Color = Color.Blu,
                reminders: Reminder[] = [],
                id: string = parseInt(Math.random() * 10000 + '', 10) + '',
              ) {
    this.id = id;
    this.name = name;

    this.color = color;
    this.reminders = reminders;
    this.reminders.push(new Reminder({name: "Test"}));
  }
}
export default List;