import React from 'react';
import {List} from '../models/List';
import '../App.css'

function SingleList (props: List) {
    return(
        <div className={"collect"}>
            <span className={"circle"} style={{backgroundColor: props.color}}> </span>
            <span className={"title"}>  {props.name}  </span>
            <span className={"detail"}> ({props.reminders.length}) </span>
        </div>
    );
}

export default SingleList;
