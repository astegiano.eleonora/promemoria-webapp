import React from 'react';
import {List} from '../models/List';
import '../App.css'
import {Reminder} from '../models/Reminder';
import {Color} from "../models/Color.enum";


function SingleReminder (props: Reminder) {
    return(
        <div className={"collect"}>
            <span className={"circle"} style={{backgroundColor: Color.Grey}}> </span>
            <span className={"title"}>  {props.name}  </span>

        </div>
    );
}

export default SingleReminder;
