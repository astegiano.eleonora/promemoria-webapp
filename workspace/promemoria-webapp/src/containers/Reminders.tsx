///al click sul singolo elenco non si apre il dettaglio,
// ma /lists/:id/reminders a cui associare un nuovo container
// Reminders che renderizza la lista dei reminders di quell'elenco
// - all'interno del container Reminders aggiungere bottone
// che porta al dettaglio dell'elenco /lists/:id con i dettagli dell'elenco,
// servirà per aggiornare l'elenco, il container è ListDetail

import React, {Component} from "react";
import Header from "../components/Header";
import {Link} from "react-router-dom";
import {Reminder} from "../models/Reminder";

interface RemindersProps {
    titleList: string;
    idList: string;
    elenchi: Reminder[];
}


export default class Reminders extends Component<RemindersProps>
{
    constructor(props: RemindersProps) {
        super(props);
    }
    render() {

        return <>
            <Header title={this.props.titleList}/>
            {this.props.elenchi.map((elenco, indice) =>

                {
                    //lists restituisce un header con nome elenchi, e poi cosa fa?
                    //lui sa che cosa è elenco perchè colection è un array di list,
                    // quindi posso eseguire il map, che manda ciascun elemento
                    //in un link, composto dalla scitta /lists+l'id della singola lista
                    //dove la chiave è sempre id

                    return <>
                        <Link to={"/lists/:id/reminders" } key={elenco.id}>
                            <div className="collect">
                                <span className="circle" style={{backgroundColor: "cadetblue"}}> </span>
                                <span>{indice}</span>
                                <span >{elenco.name}</span>
                            </div>
                        </Link>


                           </>
                }
            )
                }

            <Link to={"/lists/"+ this.props.idList}>
                <p>Dettagli lista</p>
            </Link>
                </> }; }
