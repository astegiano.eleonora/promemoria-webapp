import React from 'react';
import {List} from '../models/List';
import '../App.css';
import SingleList from "../components/SingleList";
import {Color} from "../models/Color.enum";
import {Reminder} from "../models/Reminder";
import Header from "../components/Header";
import {Link} from "react-router-dom";

interface ListsProps {
    elenchi: List[];
}
export default class Lists extends React.Component <ListsProps> {
    constructor(props: ListsProps) {
        super(props);
    }
    render() {
        return <>
            <Header title="Elenchi"/>
            {this.props.elenchi.map((elenco, indice) => {
                //lists restituisce un header con nome elenchi, e poi cosa fa?
                //lui sa che cosa è elenco perchè colection è un array di list,
                // quindi posso eseguire il map, che manda ciascun elemento
                //in un link, composto dalla scitta /lists+l'id della singola lista
                //dove la chiave è sempre id
                return <>
                <Link to={"/lists/" + elenco.id + "/reminders"} key={elenco.id}>
                    <div className="collect">
                        <span className="circle" style={{backgroundColor: elenco.color}}> </span>
                         <span>Indice oggetto nell'array: {indice}</span>
                         <span >{elenco.name}</span>
                         <span>({elenco.reminders.length})</span>
                    </div>
                </Link>


                    </>

            })}
            <Link to={"/lists/new"}>
                <span className="new-list"> Aggiungi nuova lista </span>
            </Link>
        </>
    }
}
//qui MOSTRO le liste nella schermata