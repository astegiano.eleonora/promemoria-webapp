import React from 'react';
import '../App.css';
import ShowReminder from "../ShowReminder";
import {Color} from "../models/Color.enum";
import {Reminder} from "../models/Reminder";
import List from "../models/List";
import Header from "../Header";
import ShowList from "../ShowList";


interface ListDetailProps {
    elenco?: List;
}

function ListDetail(props: ListDetailProps) {

    return props.elenco ? (
        <>
            <Header title={props.elenco.name}/>
            <p>{props.elenco.id}</p>
            {props.elenco.reminders.map((single) =>
            {return <ShowReminder name={single.name} date={single.date}
                                  done={single.done} description={single.description}/>;})}

        </>
    ) : (<p> Elenco non trovato </p>)}

    export default ListDetail;

// }
// export default class ListDetail extends React.Component <ListDetailProps> {
//     render() {
//         return <>
//             {this.props.collection.map((single, index) => {
//                 return <ShowReminder name={single.name} date={single.date} description={single.description} done={single.done}/>;
//             })}
//         </>
//     }
// }